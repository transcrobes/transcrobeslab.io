## What is it?
Transcrobes is an open source project that puts the learner at the centre of the "extensive content consumption" process. It builds a representation of the learner’s lexical (and soon grammatical) knowledge and then uses that information to enrich (gloss) real-world texts in-place as learners consume real-world content, on their preferred devices.

## Current Status
Transcrobes currently only fully supports learning Chinese for English speakers. If your English is very good, then you should be Ok but otherwise you're better to wait until your preferred language has been implemented.
