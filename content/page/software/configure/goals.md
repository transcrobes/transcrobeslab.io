---
title: Goals
comments: false
---

One of the key philosophical drivers behind Transcrobes is learner freedom. Traditional approaches typically have a very rigid idea about what every learner is like and what they should learn, and they don't particularly care that learners are actually people, rather than "scientific abstractions" or customers. Transcrobes puts each individual learner in the driver's seat. The learner knows what their macro-level goals are, and Transcrobes is here to help the learner achieve those in a fun and highly efficient way. 

Goals can be cut up into sub-goals, which are then grouped together so that they are more achievable, making it easier to get a clear idea of how the learner is progressing. For example, each chapter of a novel could be a sub-goal of the goal of reading the novel. Each chapter in a textbook could be a sub-goal of the goal of the textbook, and getting an A this year.

So key to the learning process is realtime feedback as to where learners are on their journey towards their goals. Setting goals is currently mainly achieved via [imports](/page/software/configure/imports), which can in turn be the basis of [word lists](/page/configure/wordlists). These word lists can then be used to create sub-goals and goals. Grammatical patterns, idiomatic expressions and more will be added soon.




Transcrobes parses lexical (and soon grammatical) information from the files, and the learner can then tell Transcrobes what the important features of the file are.

