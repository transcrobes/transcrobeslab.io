---
title: Glossing types
comments: false
---

Because all learners have different language backgrounds, glossing with another language may not be the best way for you to understand and learn, so a few other options are currently provided. This is one of the active research aspects of the platform - what are the best glossing techniques for learners, if they have a choice between several? How does it affect comprehension? How does it affect enjoyment? How does it affect learning?

All the options have value for learning and the happier you are reading, the more learning value it will bring over time.

By default the system also segments texts into words (where the language doesn't usually do that) but that can be turned off for each module (ebook reading, media/video or web) if you don't want that.

# Current Options

The current options available are:

| Name | Status | Comments |
|------|--------|----------|
| Unmodified Text | available | No glossing |
| Simpler words | coming soon | If the system knows of synonyms that you already know, words are glossed with those |
| Transliteration | available | The sound representation of the word (Pinyin for Chinese), great for heritage learners |
| Native Language | available | Context-based gloss in your native language |

## Unmodified Text
<img style="max-width:80%" src="/img/boocrobes/3body4.png"/>

## Segmented Text
<img style="max-width:80%" src="/img/boocrobes/3body5.png"/>

## Transliteration
<img style="max-width:80%" src="/img/boocrobes/3body6.png"/>

## Native Language
<img style="max-width:80%" src="/img/boocrobes/3body7.png"/>

## Future Glossing Modes
There are also a number of other enrichment styles that are in the pipeline. One particularly relevant for Chinese might be to only highlight compound words that it knows you know the component parts of (rather than gloss them). Or maybe a mix of all of the possible styles, depending on how common the words are and the particular part of speech. Text-to-speech will soon be available, so you can hear individual words, or even entire sentences. All these options, and many more, will become available over time and as Transcrobes gets a better picture of your knowledge, the rate you learn will increase dramatically.
