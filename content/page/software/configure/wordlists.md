---
title: Word lists
comments: false
---

Wordlists are created by importing and analysing texts, then directing the system to create a list from the imported data. Texts can be simple lists of words (CSV or TXT are currently supported, more soon), or actual content (currently EPUB ebooks and SRT/VTT subtitles formats are supported, more soon). The system stores [information from the analysis](/page/software/configure/imports) and allows
