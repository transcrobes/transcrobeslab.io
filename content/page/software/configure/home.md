---
title: Configure
comments: false
---

# Common data and configuration tasks

| Name | Task Type | Status | Comments |
|----------|-------------|------|------|
| [imports](/page/software/configure/imports) | Data/Content import | available | Supported formats: TXT, CSV, epub (v3), SRT, VTT (more soon) |
| [word lists](/page/software/configure/wordlists) | Define meaningful vocabulary/grammar sets | Vocabulary available | Grammar lists available soon |
| [goals](/page/software/configure/goals) | Define personal goals | Basic availability |  |
| [glossing](/page/software/configure/glossing) | Content glossing | available | Supported types: None, segmented, known synonym, native language (more soon) |
