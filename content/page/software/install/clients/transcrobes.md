---
title: Transcrobes website
date: 2021-04-21
comments: false
---

# Before you begin

The Transcrobes website (and [Brocrobes](/page/install/clients/brocrobes)) both use the latest web browser standard functionalities to provide for a rich, offline-capable learning and content experience. That means that even though it is entirely browser-based, you can still use most of the functionality offline, like reading ebooks, watching movies or practising vocabulary. In order to support that, Transcrobes needs to download and prepare quite a bit of data, so before you get started you will need to do that. Don't worry, you just need to sign up (or log in with the details your admin gives you) and then click Ok to initialise.

A word of warning - the initialisation process will take 10-20 minutes (depending on your device) and your device needs to do quite a bit of work, so it might heat up a bit (but probably less than a gaming session!). You should make sure your device is plugged in/charging when you do this (obligatory for Android devices - installation will stop and need to be restarted if it's not charging due to Android wanting to save your battery) and that you are on wifi (or you have a generous data plan). Initialisation will need to download around 35-50MB of data. The data files and web resources (images, javascript, html, css, etc.) need to be downloaded so you don't ever have to worry about not having a great connection. Transcrobes will resynchronise automatically when it can connect again, meaning all your devices stay in sync.


<img style="max-width:100%" src="/img/configure/initialising.png"/>


