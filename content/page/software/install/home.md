---
title: Install
date: 2021-04-21
comments: false
---

Transcrobes has both server components and client components. As the audiences for each family of components is different, documentation has been separated into two sections:

[Client installation documentation](/page/software/install/clients/home)


[Server installation documentation](/page/software/install/server/home)
