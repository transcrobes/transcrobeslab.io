---
title: Learn
date: 2021-04-21
comments: false
---

See [here](/page/meaningful-io/home) for more details on the scientific theories behind the Transcrobes project.

# Supported Activities

## Meta-learning
The development focus until now has been on making extensive content consumption tools (web, ebooks, video) available and starting to collect data. [notrobes](/page/software/learn/notrobes) (also see below) already has some basic information on what the system knows about the learner's vocabulary knowledge, including the number of times the word has been seen, definitions looked up, etc. The next phase will concentrate on giving insights to the learner on how they are progressing towards their [goals](/page/software/configure/goals).

## Real language/Content-focused activities

| Name | Activity Type | Status | Comments |
|----------|-------------|------|------|
| [brocrobes](/page/software/learn/brocrobes) | web | available |  |
| [boocrobes](/page/software/learn/boocrobes) | books (soon e-comics) | available | Some user interface usability issues are currently being improved |
| [moocrobes](/page/software/learn/moocrobes) | video | available for downloaded video, streaming coming soon |  |
| mucrobes | music | coming soon | initial support will be for a karaoke-style interface |

## Active learning-focused activities

| Name | Activity Type | Status | Comments |
|----------|-------------|------|------|
| [repetrobes](/page/software/learn/repetrobes) | vocabulary memorisation | available | Some user interface usability issues on mobile are currently being improved |
| [notrobes](/page/software/learn/notrobes) | word lookup | available | many functions available, many more planned (personalised example sentences, etc.) |
