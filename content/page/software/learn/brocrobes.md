---
title: Brocrobes
date: 2021-04-21
comments: false
---

Brocrobes is a browser extension that allows you to enrich any webpage with the your own, personalised help. When you click on the extension button, the extension will look for all text in the language you are learning on the page, and send that to the server. The server will then get context-based definitions for all the words, and translations for all the sentences, and send that back to the browser. The browser will then replace each part of the webpage with special, interactive text, including glosses (inline translations), but _only for the words you don't know_. A picture is worth a thousand words, and three are worth three thousand!

#### Original Site
<img style="max-width:50%" src="/img/brocrobes/xinhua1.png"/>
<br/>

#### After transcrobing
<img style="max-width:50%" src="/img/brocrobes/xinhua2.png"/>
<br/>

#### Popup with sentence translation showing
<img style="max-width:50%" src="/img/brocrobes/xinhua3.png"/>

## Using Brocrobes

To use brocrobes, [after you have installed it](/page/software/install/clients/browser-extensions), simply navigate to the webpage you are interested in - any webpage - and click on the web extension button `Transcrobe Me!`. Depending on the complexity and size of the webpage, it might take a few seconds for the enrichment process to start to be visible but rest assured, Transcrobes is working hard in the background.

Sometimes `brocrobes` will make a bit of a mess of a webpage, due to bad webpage coding by the site developers. The better you know the language of the webpage, the less of a mess those sites will be. The idea behind brocrobes is that if there are so many words on a page that get translated inline that it makes a mess, you definitely won't be able to understand anything on the page without it. So short of using a full webpage translation tool like Google Translate (which can make a mess too), you either have a choice of not being able to understand anything, or having the page a bit messy. It's up to you - click on the Transcrobe Me! button or not - you decide!

This will definitely get better over time, and we hope that eventually some sites will add direct support for Transcrobes. We are also thinking actively about new features that will make brocrobes even more useful - each language learner has different needs and Transcrobes wants to let each learner mix and match the exact things they need at the current point in time.

The screenshots above show glossing of the unknown words into English but you can also configure [other glossing types](/page/software/configure/glossing).
