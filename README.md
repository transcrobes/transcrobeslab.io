transcrob.es
============
This is the public documentation and information website for the Transcrobes project, made using hugo (see https://gohugo.io/).

Documentation
=============
See https://transcrob.es

Development
===========
The site is mainly informational, and is a pre-generated to static HTML. If you have "permanent" information (so not for the mailing lists) then fork, add/change, and submit a merge request.

## Developer Certificate of Origin
Please sign all your commits by using `git -s`. In the context of this project this means that you are signing the document available at https://developercertificate.org/. This basically certifies that you have the right to make the contributions you are making, given this project's licence. You retain copyright over all your contributions - this sign-off does NOT give others any special rights over your copyrighted material in addition to the project's licence.

## Contributing
See [the website](https://transcrob.es/page/contribute) for more information. Please also take a look at our [code of conduct](https://transcrob.es/page/code_of_conduct) (or CODE\_OF\_CONDUCT.md in this repo).
